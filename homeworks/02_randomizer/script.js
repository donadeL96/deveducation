var minValue = document.querySelector(".minValue"),
    maxValue = document.querySelector(".maxValue"),
    buttonGetValue = document.querySelector('.getRandomValue'),
    buttonResetValues = document.querySelector('.resetValues'),
    increment = document.querySelector('.increment'),
    decrement = document.querySelector('.decrement'),
    countOfRandomValue = document.querySelector('.countOfRandomValue'),
    placeForRandomValue = document.getElementsByClassName('placeForRandomValue')[0],
    count = 1;

window.onload = function(){
    countOfRandomValue.value = count;
};

function getRandomValue(){
    if(!isNaN(minValue.value) && !isNaN((maxValue.value)) && !isNaN((countOfRandomValue.value))){
        var minNumber = Math.ceil(minValue.value),
            maxNumber = Math.floor(maxValue.value),
            result1 = '';
        for(var i = 0; i < count; i++) {
            var result = Math.floor(Math.random() * (maxNumber - minNumber + 1)) + minNumber;
            result1 += result + '<br />';
        }
        placeForRandomValue.innerHTML = result1;
    }else{
        if(isNaN(minValue.value)){
            minValue.value = "";
            minValue.placeholder = "You need to enter a number";
        }if(isNaN(maxValue.value)){
            maxValue.value = "";
            maxValue.placeholder = "You need to enter a number";
        }
        if(isNaN(countOfRandomValue.value)){
            countOfRandomValue.value = "";
            countOfRandomValue.placeholder = "You need to enter a number";
        }
    }
}

increment.addEventListener('click', function(){
   count++;
   countOfRandomValue.value = count;
});

decrement.addEventListener('click', function(){
    if(count > 1){
        count--;
        countOfRandomValue.value = count;
    }
});

countOfRandomValue.addEventListener('change', function(){
    count = countOfRandomValue.value;
});

buttonGetValue.addEventListener('click', getRandomValue);

buttonResetValues.addEventListener('click', function(){
    minValue.value = '';
    minValue.placeholder = "";
    maxValue.value = '';
    maxValue.placeholder = "";
    countOfRandomValue.value = '1';
    count = 1;
    placeForRandomValue.innerHTML = "";
});

// module.exports = {
//
// };

