import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";

class App extends Component{

    constructor(){
        super();
        this.state = {
            sec: 0,
            finalSeconds: '00',
            finalMinutes: '00',
            timeOut: null,
        };
    }

    componentDidMount(){
        document.getElementById("btnRun").disabled = true;
        document.getElementById("btnSet").disabled = false;
    }

    stopTimer(){
        if(this.state.timeOut){
            clearTimeout(this.state.timeOut);
            document.getElementById("btnRun").disabled = false;
        }
    }

    resetTimer(){
        let btnRun = document.getElementById("btnRun");
        let btnSet = document.getElementById("btnSet");
        this.stopTimer();
        document.getElementById("input_timer").value = 0;
        this.setState({
            sec: 0
        }, this.countTime);
        btnRun.disabled = true;
        btnSet.disabled = false;
    }

    setTimer(){
        let btnRun = document.getElementById("btnRun");
        let input = document.getElementById("input_timer").value;
        let param = parseInt(input);
        this.setState({
            sec: param,
        }, this.countTime);
        btnRun.disabled = false;
    }

    runTimer(){
        let btnSet = document.getElementById("btnSet");
        let input = document.getElementById("input_timer").value;
        document.getElementById("btnRun").disabled = true;
        let param = parseInt(input) + 1;
        console.log("param runTimer", param);
        this.decrement.bind(this, param)();
        btnSet.disabled = true;
    }

    decrement(data){
        console.log("параметр в декремент", data);
        this.setState({
            sec: data - 1,
        }, console.log("cостояние секунды в стейте", this.state.sec));
        console.log("cостояние секунды", this.state.sec);
        if(this.state.sec > 0) {
            this.setState({
                timeOut: setTimeout(this.decrement.bind(this, this.state.sec), 1000),
            });
        }
        this.countTime();
    };

    countTime(){
        let secondsForVal = this.state.sec % 60;
        let minutesForVal = Math.floor(this.state.sec / 60);

        if(secondsForVal.toString().length < 2){
            secondsForVal = '0' + secondsForVal.toString();
        }
        if(minutesForVal.toString().length < 2){
            minutesForVal = '0' + minutesForVal.toString();
        }

        this.setState({
            finalSeconds: secondsForVal,
            finalMinutes: minutesForVal,
        });

    }

    render(){
        return(
            <div className="App">
                <div className="input_timer"><input type="text" className="input_value" id="input_timer"/></div>
                <div className="timer">
                    <div className="minutesBlock">{this.state.finalMinutes}:</div>
                    <div className="secondsBlock">{this.state.finalSeconds}</div>
                </div>
                <button onClick={this.runTimer.bind(this)} id="btnRun">Запустить</button>
                <button onClick={this.setTimer.bind(this)} id="btnSet">Установить</button>
                <button onClick={this.stopTimer.bind(this)} id="btnStop">Стоп</button>
                <button onClick={this.resetTimer.bind(this)} id="btnReset">Сброс</button>
            </div>
        );
    }
}

export default hot(module)(App);
