export default function changeName(lg) {
    let phrases = {
        "wrapper-modal__name": {
            "RU": "Настройки",
            "EN": "Settings",
            "AR": "إعدادات"
        },
        "cin-number__name": {
            "RU": "Введите значение",
            "EN": "Enter value",
            "AR": "أدخل رقم"
        },
        "select__option_item1": {
            "RU": "Метры",
            "EN": "Meters",
            "AR": "متر"
        },
        "select__option_item2": {
            "RU": "Мили",
            "EN": "Miles",
            "AR": "ميل"
        },
        "select__option_item3": {
            "RU": "Футы",
            "EN": "Foots",
            "AR": "قدم"
        },
        "select__option_item4": {
            "RU": "Ярды",
            "EN": "Yards",
            "AR": "ياردة"
        },
        "select__option_item5": {
            "RU": "Версты",
            "EN": "Versts",
            "AR": "معالم"
        },
        "wrapper-modal__name1": {
            "RU": "Метры",
            "EN": "Meters",
            "AR": "متر"
        },
        "wrapper-modal__name2": {
            "RU": "Мили",
            "EN": "Miles",
            "AR": "ميل"
        },
        "wrapper-modal__name3": {
            "RU": "Футы",
            "EN": "Foots",
            "AR": "قدم"
        },
        "wrapper-modal__name4": {
            "RU": "Ярды",
            "EN": "Yards",
            "AR": "ياردة"
        },
        "wrapper-modal__name5": {
            "RU": "Версты",
            "EN": "Versts",
            "AR": "معالم"
        },
        "changeValue__button": {
            "RU": "Сброс",
            "EN": "Reset",
            "AR": "إعادة تعيين"
        },
        "getResult__button": {
            "RU": "Конвертировать",
            "EN": "Convert",
            "AR": "تحويل"
        },
        "title": {
            "RU": "Конвертор",
            "EN": "Convertor",
            "AR": "محول"
        },
        "reset__button": {
            "RU": "Сбросить",
            "EN": "Clear",
            "AR": "نظيف"
        },
        "container_button__error-correct": {
            "RU": "Заполните коректно поле ввода",
            "EN": "Fill in the input field correctly",
            "AR": "املأ حقل الإدخال بشكل صحيح"
        },
        "container_button__error-negativeNumber": {
            "RU": "Вы ввели отрицательное значение",
            "EN": "You entered a negative number",
            "AR": "لقد أدخلت رقما سالبا"
        }

    };

    let all_elements = document.querySelectorAll('[data-id_phrase]');
    for (let j in phrases) {
        for (let i = 0; i < all_elements.length; i++) {
            if (all_elements[i].dataset.id_phrase === j) {
                all_elements[i].innerHTML = phrases[j][lg];
            }
        }
    }
}