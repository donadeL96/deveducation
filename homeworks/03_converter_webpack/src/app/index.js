import '../style/app.less';
import changeLang from "./changeLang";

let selectedLeftValue = document.querySelector('.left-select'),
    selectedRightValue = document.querySelector('.right-select'),
    inputNumber = document.getElementById('valueToConvert'),
    result = document.getElementById('result'),
    getResult = document.querySelector('.container_button__getResult'),
    reset = document.querySelector('.container_button__reset'),
    errorMessageCorrect = document.querySelector('.container_button__error-correct'),
    errorMessageNumber = document.querySelector('.container_button__error-negativeNumber'),
    valueLeft,
    res = 0,
    valueRight;

selectedLeftValue.options[0].selected = true;
selectedRightValue.options[4].selected = true;
valueLeft = selectedLeftValue.value;
valueRight = selectedRightValue.value;
result.placeholder = res;
inputNumber.placeholder = res;
function countResult(){
    if(!isNaN(+inputNumber.value) && +inputNumber.value.length <= 13 && inputNumber.value !== '') {
        if (+inputNumber.value >= 0) {
            res = +inputNumber.value;
            switch (valueLeft) {
                case "miles":
                    res *= 1609.344;
                    break;
                case "versts":
                    res *= 1066.8;
                    break;
                case "yards":
                    res *= 0.9144;
                    break;
                case "foots":
                    res *= 0.3048;
            }
            switch (valueRight) {
                case "miles":
                    res *= 0.00062137119223733;
                    break;
                case "foots":
                    res *= 3.2808398950131;
                    break;
                case "yards":
                    res *= 1.0936132983377;
                    break;
                case "versts":
                    res *= 0.0009373828271466067;
                    break;
            }
            errorMessageNumber.classList.add('error');
            result.value = res.toPrecision(3).toString();
        }else{
            errorMessageCorrect.classList.add('error');
            errorMessageNumber.classList.remove('error');
            result.value = 0;
        }
        errorMessageCorrect.classList.add('error');
    }else{
        errorMessageNumber.classList.add('error');
        errorMessageCorrect.classList.remove('error');
        result.value = 0;
    }
}

reset.addEventListener('click', () => {
    res = 0;
    inputNumber.value = '';
    result.value = "";
    result.placeholder = res;
    inputNumber.placeholder = res;
});

getResult.addEventListener('click', countResult);

selectedLeftValue.addEventListener('click', function(){
    if(selectedLeftValue.options.selectedIndex){
        selectedLeftValue[selectedLeftValue.options.selectedIndex].classList.toggle('error');
    }
    valueLeft = selectedLeftValue.value;
});

selectedRightValue.addEventListener('click', function(){
    if(selectedRightValue.options.selectedIndex){
        selectedRightValue[selectedRightValue.options.selectedIndex].classList.toggle('error');
    }
    valueRight = selectedRightValue.value;
});

// переключение языков

window.onload = function(){
    changeLang(localStorage.getItem('language'));
    list.options[localStorage.getItem('numberOfOption')].selected=true;
};

let modal = document.getElementById('modal'),
    setting = document.getElementById('setting'),
    list = document.querySelector('.list'),
    close = document.getElementById('close');

setting.addEventListener('click', () => {
    setting.classList.add('close-window');
    modal.classList.remove('close-window');
});

close.addEventListener('click', () => {
    modal.classList.add('close-window');
    setting.classList.remove('close-window');
});

list.addEventListener('click', () =>{
    localStorage.setItem('numberOfOption', list.options.selectedIndex);
    localStorage.setItem('language', list.value);
    changeLang(localStorage.getItem('language'));
});







