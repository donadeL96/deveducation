let users;

fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(response => {
        users = response;
        // useForEach();
        // useSort();
        // useMap();
        // useFilter();
        // useReduce();
    });

function useForEach(){
    let list = document.getElementById('list');
    users.forEach(function(item, index, array){
        let dtElement = document.createElement('dt');
        dtElement.innerHTML = array[index].username;
        list.append(dtElement);
        let ddElement = document.createElement('dd');
        ddElement.innerHTML = array[index].name;
        list.append(ddElement);
    })
}
function useSort(){
    let names = users.sort(function(firstName, secondName){
        let nameA = firstName.username.toUpperCase();
        let nameB = secondName.username.toUpperCase();

        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    });

    console.log(names);
}

function useMap(){
    let names = users.map(function(item, index, array){
        return array[index].name.toUpperCase();
    });
    console.log(names);
}

function useFilter(){
    let names = users.filter(function(item, index, array){
        return array[index].username.length > 10;
    });
    console.log(names);
}

function useReduce() {
    let finalSum = users.reduce((sum, current) => {
        let nameLength1 = current.name;
        let nameLength2 = sum.name;
        // console.log(nameLength1.length);
        // console.log(nameLength2.length);
        // console.log(typeof nameLength1);
        // console.log(typeof nameLength2);
        return nameLength2 + nameLength1;
    });

    console.log(finalSum);
}

































// let FN = [];
//
// fetch('https://reqres.in/api/users?page=2')
//     .then(res => {
//         return res.json();
//     }).then(data => {
//         FN.push(data.data);
//     });
//
// console.log("Привет", FN[0]);
//
// function useForEach(){
//     let list = document.getElementsByClassName('list')[0];
//     console.log(FN);
//     let example = FN;
//     console.log("example", example);
//     // FN.forEach((item, index, array) => {
//     //     let dtElem = document.createElement('dt');
//     //     dtElem.innerHTML = 'append';
//     //     list.append(dtElem);
//     // });
//     return FN;
// }
//
// useForEach();
// // function useSort(){
// //     FN.sort();
// //     console.log("sort",FN);
// //     return FN;
// // }
// //
// // function useFilter(){
// //
// // }
