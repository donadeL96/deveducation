var IList = function () {};

//ArrayList
var ArrayList = function() {
    IList.apply(this, arguments);
    this.array = [];
};

ArrayList.prototype.push = function(value) {
    this.array[this.array.length] = value;
    return this.array;
};

ArrayList.prototype.getSize = function(value) {
    return this.array.length;
};

ArrayList.prototype.init = function(initialArray) {
    for (var i = 0; i < initialArray.length; i++) {
        this.push(initialArray[i]);
    }
    return this.array;
};

ArrayList.prototype.toString = function() {
    let arrayString = "";
    for(let i = 0; i < this.array.length; i++){
        arrayString += this.array[i];
    }
    return arrayString;
};

ArrayList.prototype.pop = function(){
    let returnNumber = this.array[this.array.length - 1];
    this.array.length = this.array.length - 1;
    return returnNumber;
};

ArrayList.prototype.shift = function(){
    let firstElement = this.array[0];
    let finalArray = [];
    for(let i = 1; i < this.array.length; i++){
        finalArray.push(this.array[i]);
    }
    this.array = finalArray;
    return firstElement;
};

ArrayList.prototype.unshift = function(){
    let array1 = [];
    for(let i = 0; i < arguments.length; i++){
            array1[i] = arguments[i];
    }
        for (let h = array1.length, m = 0; h < (this.array.length + array1.length), m < this.array.length; h++, m++) {
            array1[h] = this.array[m];
        }
    this.array = array1;
    return this.array;
};

ArrayList.prototype.slice = function(start, end) {
    var sliceArray = [];
    var spliceEnd = end || this.array.length;
    for (var i = start; i < spliceEnd; i++) {
        sliceArray.push(this.array[i]);
    }
    return sliceArray
};

ArrayList.prototype.splice = function(){
    let array1 = [];
    let countOfArg = arguments.length;
    let count = 0;
        if(arguments[0] >= 0){
            for(let i = 0; i < this.array.length ;i++){
                if(i < arguments[0] || i > arguments[1]){
                    array1[count] = this.array[i];
                    count++;
                }else if(countOfArg > 2){
                    for(let j = 2; j < arguments.length; j++) {
                        array1[count] = arguments[j];
                        count++;
                    }
                    countOfArg = 0;
                }
            }
        }
    this.array = array1;
    return this.array;
};

ArrayList.prototype.sort = function() {
    for (let i = 0; i < this.array.length - 1; i++) {
        for (let j = 0; j < this.array.length - 1 - i; j++) {
            if (this.array[j] > this.array[j+1]) {
                let temp = this.array[j+1];
                this.array[j+1] = this.array[j];
                this.array[j] = temp
            }
        }
    }
    return this.array
};

ArrayList.prototype.get = function(index) {
    return this.array[index];
};

ArrayList.prototype.set = function(index, element) {
    this.array[index] = element;
    return this.array;
};

// LinkedList
let Node = function(value) {
    this.value = value;
    this.next = null;
};
let LinkedList = function() {
    IList.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(IList.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    let tempNode = this.root;
    let size = 0;
    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    console.log("size", size);
    return size;
};

LinkedList.prototype.unshift = function(value) {
    let size = this.getSize();
    let node = new Node(value);
    node.next = this.root;
    this.root = node;
    console.log("unshift", this);
    return size + 1;
};

LinkedList.prototype.toString = function(){
    let str = '';
    let tempNode = this.root;
    while(tempNode !== null){
        str += tempNode.value;
        tempNode = tempNode.next;
    }
    console.log("toString", str);
    return str;
};


LinkedList.prototype.init = function(initialArray) {
    for (let i = initialArray.length -1 ; i >=0; i--) {
        this.unshift(initialArray[i]);
    }
    console.log("init", this);
};

LinkedList.prototype.push = function(value){
    let size = this.getSize();
    let node = new Node(value);
    let tempNode = this.root;
    while(tempNode.next !== null){
        tempNode = tempNode.next;
    }
    tempNode.next = node;
    size++;
    console.log("push", this);
    return size;
};

LinkedList.prototype.pop = function(){
    let firstElement;
    let size = this.getSize();
    let tempNode = this.root;
    while(tempNode.next !== null){
        firstElement = tempNode;
        tempNode = tempNode.next;
    }
    firstElement.next = null;
    tempNode = null;
    size--;
    console.log("pop", this);
    return size;
};

LinkedList.prototype.shift = function(){
    let size = this.getSize();
    let tempNode = this.root;
    this.root = tempNode.next;
    tempNode = null;
    size--;
    console.log("shift", this);
    return size;
};

LinkedList.prototype.slice = function(start, end){
    let newArray = [];
    let count_of_operations = 0;
    let count_of_element = 0;
    let tempNode = this.root;

    while(tempNode.next !== null){
        tempNode = tempNode.next;
        count_of_operations++;
        if(count_of_operations >= start){
            count_of_element++;
            if(count_of_element < end){
                newArray[newArray.length] = tempNode.value;
            }
        }
    }
    console.log("slice", newArray)
    return newArray;
};

LinkedList.prototype.get = function(index){
    let count = 0;
    let result;
    let tempNode = this.root;
    while(tempNode.next !== null){
        count++;
        tempNode = tempNode.next;
        if(count === index){
            result = tempNode;
        }
    }
    console.log("get", result);
    return result;
};

LinkedList.prototype.set = function(index, element){
    let size = this.getSize();
    let count = 0;
    let tempNode = this.root;
    let node = new Node(element);
    while(tempNode.next != null){
        count++;
        if(count + 1 === index){
            node.next = tempNode.next;
            tempNode.next = node;
        }else {
            tempNode = tempNode.next;
        }
    }
    return size + 1;
};

let list = new LinkedList();

list.init([1, 2, 3, 4]);
list.getSize();
list.unshift(9);
list.toString();
list.push(9);
list.pop();
list.shift();
list.slice(1, 3);
list.get(2);
list.set(2 , 'ghbdtn');

module.exports = {
  arrayList: ArrayList,
};
