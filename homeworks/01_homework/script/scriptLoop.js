module.exports = {
//ЦИКЛЫ

//1) Найти сумму четных чисел и их количество в диапазоне от 1 до 99

    sumEvenNumbers: function(from, to){
        if (typeof from === "number" && typeof to === "number") {
            var quantity = 0,
                sum = 0;
            for (var i = from; i <= to; i++) {
                if (i % 2 === 0) {
                    quantity++;
                    sum += i;
                }
            }
            return {quantity: quantity, sum: sum};
        }
    },


//2) Проверить простое ли число?

    simpleNumber: function(number){
        if (typeof number === "number") {
            let result;
            for (let i = 1; i < number; i++) {
                if(number % i !== 0) {
                    result = false;
                } else {
                    result = true;
                }
            }
            return result;
        }
    },

//3) Найти корень натурального числа с точностью до целого.

    findSqrt: function(number){
        if (typeof number === "number") {
            let x = 1;
            let result = 0;

            while (number > 0) {
                number -= x;
                x += 2;
                result += (number < 0) ? 0 : 1;
            }
            return result;
        }
    },

//4) Вычислить факториал числа n

    factorial: function(number){
        if (typeof number === "number") {
            var count = 1;

            for (var i = 1; i <= number; i++) {
                count *= i;
            }
            return count;
        }
    },

//5) Посчитать сумму цифр заданного числа

    sumNumbers: function(number) {
        if (typeof number === "number") {
            let result = 0;

            while (number > 0) {
                let n = number % 10;
                result += n;
                number -= n;
                number /= 10;
            }
            return result;
        }
    },


//6) Зеркальное отображение числа

    mirrorNumber: function(number){
        if (typeof number === "number") {
            let mirror = 0;

            while (number > 0) {
                let n = number % 10;
                mirror += n;
                mirror *= 10;
                number -= n;
                number /= 10;
            }
            mirror /= 10;
            return mirror;
        }
    }
};


