module.exports = {
// ОДНОМЕРНЫЕ МАССИВЫ

//1)Минимальный елемент массива

    findMinElement: function(array){
        if (typeof array === "object") {
            var minValue = 0,
                result = 0;

            for (var i = 1; i < array.length; i++) {
                if (array[minValue] > array[i]) {
                    minValue = i;
                    result = array[i];
                } else if (array[minValue] < array[i]) {
                    result = array[minValue];
                }
            }
            return result;
        }
    },


// 2) Найти максимальный елемент массива

findMaxElement: function(array){
    if (typeof array === "object") {
        var maxValue = 0,
            result = 0;

        for (var i = 1; i < array.length; i++) {
            if (array[maxValue] > array[i]) {
                result = array[maxValue];
            } else if (array[i] > array[maxValue]) {
                maxValue = i;
                result = array[i];
            }
        }
        return result;
    }
},

//3)Найти индекс минимального элемента в массиве

findMinIndex:function(array){
    if (typeof array === "object") {
        var findIndex = 0;

        for (var i = 1; i < array.length; i++) {
            if (array[i] < array[findIndex]) {
                findIndex = i;
            }
        }
        return findIndex;
    }
},


//4)Найти индекс максимального элемента в массиве

findMaxIndex:function(array){
    if (typeof array === "object") {
        var findIndex = 0;

        for (var i = 1; i < array.length; i++) {
            if (array[i] > array[findIndex]) {
                findIndex = i;
            }
        }
        return findIndex;
    }
},


//5) Посчитать сумму елементов массива с нечетными индексами

sumOfOddIndex: function(array) {
    if (typeof array === "object") {
        var count = 0,
            i = 1;

        while (i < array.length) {
            count += array[i];
            i += 2;
        }
        return count;
    }
},


//6) Сделать реверс массива

revArray: function(array) {
    if (typeof array === "object") {
        var temp = [];
        for (var i = 0, j = array.length - 1; i < array.length, j >= 0; i++, j--) {
            temp[i] = array[j];
        }
        return temp;
    }
},


//7) Посчитать количество нечетных елементов в массиве.

countOddElements: function(array){
    if (typeof array === "object") {
        var count = 0;

        for (var i = 0; i < array.length; i++) {
            if (array[i] % 2 !== 0) {
                count++;
            }
        }
        return count;
    }
},


//8)Поменять местами первую и вторую половину массива,например для массива 1234 результат 3412.

changePlaces: function(array){
    if (typeof array === "object") {
        var temp = 0;
        for (var i = 0, j = array.length / 2; i <= array.length / 2, j <= array.length - 1; i++, j++) {
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
},


//9) Отсортировать массив(пузырьком(Bubble), выбором(Select), вставками(Insert)).

bubble: function(array){
    if (typeof array === "object") {
        var temp;

        for (var i = array.length - 1; i >= 0; i--) {
            for (var j = 0; j < i; j++) {
                if (array[j] < array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
},


select: function (array) {
    if (typeof array === "object") {
        for (var i = 0; i < array.length; i++) {
            var maxIndex = i;
            for (var j = i + 1; j < array.length; j++) {
                if (array[maxIndex] < array[j]) {
                    maxIndex = j;
                }
            }
            if (maxIndex !== i) {
                var temp = array[i];
                array[i] = array[maxIndex];
                array[maxIndex] = temp;
            }
        }
        return array;
    }
},
    

insertArray:function(array){
    if (typeof array === "object") {
        for (let i = 1; i < array.length; i++) {
            let current = array[i];

            while (i > 0 && array[i - 1] > current) {
                array[i] = array[i - 1];
                i--;
            }
            array[i] = current;
        }
        return array;
    }
},

};
