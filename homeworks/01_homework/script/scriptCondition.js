module.exports = {

// УСЛОВНЫЕ ОПЕРАТОРЫ

//1) Если а - четное посчитать а*b, иначе a+b

evenOrOdd: function(a, b) {
    if (typeof a === "number" && typeof b === "number") {
        let result = 0;
        if (a % 2 === 0) {
            result = a * b;
        } else {
            result = a + b;
        }
        return result;
    }
},


//2) пределить какой четверти принадлежит точка с координатами(x, y)

coordination: function(x, y) {
    if (typeof x === "number" && typeof y === "number") {
        var result = 0;
        if (x > 0 && y > 0) {
            result = 1;
        }else if(x < 0 && y > 0){
            result = 2;
        }else if(x < 0 && y < 0){
            result = 3;
        }else if(x > 0 && y < 0){
            result = 4;
        }else if((y > 0 || y < 0) && (x == 0)){
            result = 'Y';
        }else if((x > 0 || x < 0) && (y == 0)){
            result = 'X';
        }
        return result;
    }
},


//3) Найти суммы только положительных из трёх чисел

sum: function(array) {
    if (typeof array === "object") {
        var finalSum = 0;
        for (var i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                finalSum += array[i];
            }
        }
        return finalSum;
    }
},


//4) Посчитать выражение max(a*b*c, a+b+c)+3;

max: function (a, b, c){
    if (typeof a === "number" && typeof b === "number" && typeof c === "number") {
        var result = 0;

        if ((a * b * c) > (a + b + c)) {
            result = (a * b * c) + 3;
        } else if ((a * b * c) < (a + b + c)) {
            result = (a + b + c) + 3;
        } else {
            result = (a + b + c) + 3;
        }

        return result;
    }
},


// 5) Найти оценку по рейтингу

defineMark: function(mark) {
    if (typeof mark === "number") {
        var result;
        if (mark >= 0 && mark <= 19) {
            result = 'F';
        } else if (mark >= 20 && mark <= 39) {
            result = 'E';
        } else if (mark >= 40 && mark <= 59) {
            result = 'D';
        } else if (mark >= 60 && mark <= 74) {
            result = 'C';
        } else if (mark >= 75 && mark <= 89) {
            result = 'B';
        } else if (mark >= 90 && mark <= 100) {
            result = 'A';
        } else {
            result = 'Вы ввели неправильный рейтинг!';
        }

        return result;
    }
}
};