var assert = require('chai').assert,
    evenOrOdd = require('../script/scriptCondition.js').evenOrOdd,
    coordination = require('../script/scriptCondition.js').coordination,
    sum = require('../script/scriptCondition.js').sum,
    max = require('../script/scriptCondition.js').max,
    defineMark = require('../script/scriptCondition.js').defineMark;


describe("homework1 testing conditional statements", function() {

//1) Если а - четное посчитать а*b, иначе a+b

describe("function evenOrOdd", function() {
    it("should return multiply if parameters equal to 2 and 3", function() {
        assert.equal(evenOrOdd(2,3), 6);
    });
    it("should return multiply if parameters equal to 5 and 3", function() {
        assert.equal(evenOrOdd(5,3), 8);
    });
    it("should return error of function", function() {
        assert.equal(evenOrOdd({},""), void 0);
    });
});

//2) пределить какой четверти принадлежит точка с координатами(x, y)

describe("function coordination", function() {
    it("should return the first quarter", function() {
        assert.equal(coordination(2,3), 1);
    });
    it("should return the second quarter", function() {
        assert.equal(coordination(-5,3), 2);
    });
    it("should return the third quarter", function() {
        assert.equal(coordination(-1,-3), 3);
    });
    it("should return the fourth quarter", function() {
        assert.equal(coordination(5,-1), 4);
    });
    it("should return axis Oy", function() {
        assert.equal(coordination(0,-5), 'Y');
    });
    it("should return axis Ox", function() {
        assert.equal(coordination(-5, 0), 'X');
    });
    it("should return error of function", function() {
        assert.equal(evenOrOdd({},""), void 0);
    });
});


//3) Найти суммы только положительных из трёх чисел

describe("function sum", function() {
    it("should return sum of 1 and 3", function() {
        assert.equal(sum([1, -2, 3]), 4);
    });
    it("should return number 2", function() {
        assert.equal(sum([-2, 2, -3]), 2);
    });
    it("should return error of function", function() {
        assert.equal(sum(45), void 0);
    });
    it("should return sum of empty array 0", function() {
        assert.equal(sum([]), 0);
    });
});


//4) Посчитать выражение max(a*b*c, a+b+c)+3;

describe("function max", function() {
    it("should return sum of only positive of three numbers", function() {
        assert.equal(max(-1, 7, -1), 10);
    });
    it("should return sum of only positive of three numbers", function() {
        assert.equal(max(0, 2, 7), 12);
    });
    it("should return sum of only positive of three numbers", function() {
        assert.equal(max(1, 2, 3), 9);
    });
    it("should nothing return because typeof !== number", function() {
        assert.equal(sum({}, [], ''), 0);
    });
});

// 5) Найти оценку по рейтингу

describe("function defineMark", function() {
    it("should return mark F if rate from 0 to 19", function() {
        assert.equal(defineMark(18), 'F');
    });
    it("should return mark E if rate from 20 to 39", function() {
        assert.equal(defineMark(30), 'E');
    });
    it("should return mark D if rate from 40 to 59", function() {
        assert.equal(defineMark(50), 'D');
    });
    it("should return mark C if rate from 60 to 74", function() {
        assert.equal(defineMark(70), 'C');
    });
    it("should return mark B if rate from 75 to 89", function() {
        assert.equal(defineMark(80), 'B');
    });
    it("should return mark A if rate from 90 to 100", function() {
        assert.equal(defineMark(95), 'A');
    });
    it("should return mark A if rate from 90 to 100", function() {
        assert.equal(defineMark(95), 'A');
    });
    it("should return error of function", function() {
        assert.equal(defineMark(''), void 0);
    });
});

});
