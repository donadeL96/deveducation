var assert = require('chai').assert,
    sumEvenNumbers = require('../script/scriptLoop.js').sumEvenNumbers,
    factorial = require('../script/scriptLoop.js').factorial,
    mirrorNumber = require('../script/scriptLoop.js').mirrorNumber,
    simpleNumber = require('../script/scriptLoop.js').simpleNumber,
    findSqrt = require('../script/scriptLoop.js').findSqrt,
    sumNumbers = require('../script/scriptLoop.js').sumNumbers;

describe("homework2 testing loops", function() {

    //1) Найти сумму четных чисел и их количество в диапазоне от 1 до 99

    describe("function sumEvenNumbers", function() {
        it("should return the sum of even numbers and their quantity in the range from 1 to 10", function () {
            assert.isObject(sumEvenNumbers(1, 10), {quantity: 5, sum: 30});
        });
        it("should return the sum of even numbers and their quantity in the range from 10 to 25", function () {
            assert.isObject(sumEvenNumbers(10, 25), {quantity: 8, sum: 136});
        });
        it("should return error of function sumEvenNumbers", function() {
            assert.equal(sumEvenNumbers('', {}), void 0);
        });
    });

    //2) Проверить простое ли число?

    describe("function simpleNumber", function() {
        it("should return true of the 3 is simple number", function () {
            assert.isBoolean(simpleNumber(3), true);
        });
        it("should return true of the 2 is simple number", function () {
            assert.isBoolean(simpleNumber(2), true);
        });
        it("should return false of the 10 is simple number", function () {
            assert.isBoolean(simpleNumber(10), false);
        });
        it("should return error of function simpleNumber", function () {
            assert.equal(simpleNumber(''), void 0);
        });
    });

    //3) Найти корень натурального числа с точностью до целого.

    describe("function findSqrt", function() {
        it("should return sqrt of 4 that is 2", function () {
            assert.equal(findSqrt(4), 2);
        });
        it("should return sqrt of 9 that is 3", function () {
            assert.equal(findSqrt(9), 3);
        });
        it("should return sqrt of 16 that is 4", function () {
            assert.equal(findSqrt(16), 4);
        });
        it("should return error of function findSqrt", function () {
            assert.equal(simpleNumber(''), void 0);
        });
    });

    //4) Вычислить факториал числа n

    describe("function factorial", function() {
        it("should return the factorial of 5", function () {
            assert.equal(factorial(5), 120);
        });
        it("should return the factorial of 6", function () {
            assert.equal(factorial(6), 720);
        });
        it("should return error of function factorial", function () {
            assert.equal(simpleNumber({}), void 0);
        });
    });

    //5) Посчитать сумму цыфр заданного числа

    describe("function sumNumbers", function() {
        it("should return the sum of numbers of 123 is 6", function () {
            assert.equal(sumNumbers(123), 6);
        });
        it("should return the sum of numbers of 1234 is 10", function () {
            assert.equal(sumNumbers(1234), 10);
        });
        it("should return the sum of numbers of 9872 is 26", function () {
            assert.equal(sumNumbers(9872), 26);
        });
        it("should return error of function sumNumbers", function () {
            assert.equal(simpleNumber('{}'), void 0);
        });
    });

    //6) Зеркальное отображение числа

    describe("function mirrorNumber", function() {
        it("should return a mirrored number of 123 that is 321", function () {
            assert.equal(mirrorNumber(123), 321);
        });
        it("should return a mirrored number of 1234 that is 4321", function () {
            assert.equal(mirrorNumber(1234), 4321);
        });
        it("should return a mirrored number of 9872 that is 2789", function () {
            assert.equal(mirrorNumber(9872), 2789);
        });
        it("should return error of function mirrorNumber", function () {
            assert.equal(simpleNumber([]), void 0);
        });
    });

});